# Swabs Rapid Prototyping

## Swabs Extruder
- Project Link: https://gitlab.cba.mit.edu/alfonso/swab-extruder
  
![Extruder](img/extrusion.png)

## Hair STL Ecporter

- Demo Link: https://amiraa.pages.cba.mit.edu/BioPrintingDesigner/01_Code/hairPrinter/exporter.html
- A tool to export STL (and later SLC) files to test the vaiability of using CBA's Stratasys Object260 Connex3 printer for printing hair like structures.

![Hair STL Ecporter](img/hair_exporter.png)


## Objet swab texture testing

first objet test for "hair" or swab texture.  Pillars .004" in diameter, .01" tall with .0025" spacing between them.
not in the right ballpark, texture is barely detectable.

  
![](img/swabarray.png)

Amira's tool for generating patterns is great but I quickly went out of the value range with the XY size, as well as the feature size of the pillars.  I'm still throttled by cad performance, and have dropped grids from 70x70 to 35x35 to relieve that

I was able to obtain the file Formlabs is using to produce swabs. I printed one and attempted to measure the features.  

![](img/swab_print_3_30.png)

This gave me some reference and today before leaving the lab i started an array of pillars .02" diameter, .03" tall with .015" spacing between.

## FDM Approaches

- Jack Forman have been leading the effort to see how we can use FDM printers to print har like structures.
- The first suggestions is to follow an approach similar to the "furbrication" project: http://www.gierad.com/projects/furbrication/
  
![image from: http://www.gierad.com/assets/3dprintedhair/furbrication.pdf](img/fubrication.png)
  
- Another idea Jack just tested is to use his 3-D Printed textiles approach to 3D print pseudo-foams (basically under extruding volumes instead of sheets). 
  
![](img/PastedGraphic.png)

- This is a photo of the first attempt with PLA. There are some quick optimization that can be made to this (extrude even less to make more porous, print inside out, etc.); however, the ultimate issue is the PLA is too crunchy/brittle, there is a possibility it migh break off in someone nose.

- Jack will be moving next onto **TPU**, which he had  luck printing super tough flexible textiles with [in the past](https://vimeo.com/402038430/72c40e2c65)  (since that video he has gotten the pattern to be more homogenous). The biocompatibility is not as good as PLA, but we do not know how much of an issue that is since its a quick swab.

![](img/tpu.png)


## Jifei's STL
- [Formlabs response](https://formlabs.com/covid-19-response/covid-test-swabs/)

![Formlabs Swabs](img/formlabs-tests.png)

- Link for the Swab Instron Tests Project: https://gitlab.cba.mit.edu/alfonso/swab-testing-for-covid-19

![](img/images_test.gif)

- Jifei said their STL design has been tested at Beth Israel, and Formlabs is getting ready to scale that. We could add our Form printers to that fleet, I'll ping Max.

## Foams
- In Neil's review of fab lab projects, there was a note that foams work well for swab sample collection. There might be a quick approach to stamp out foam cylinders.